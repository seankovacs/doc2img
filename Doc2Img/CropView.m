//
//  CropView.m
//  Doc2Img
//
//  Created by Sean Kovacs on 8/18/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "CropView.h"

@implementation CropView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super initWithCoder:aDecoder]) {
        self.contentMode = UIViewContentModeRedraw;
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor greenColor].CGColor);
    CGContextStrokeRectWithWidth(ctx, CGRectInset(rect, 5.0f, 5.0f), 2.0f);
}

@end
