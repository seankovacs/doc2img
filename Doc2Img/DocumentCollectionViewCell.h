//
//  DocumentCollectionViewCell.h
//  Doc2Img
//
//  Created by Sean Kovacs on 8/17/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentCollectionViewCell : UICollectionViewCell
@property (nonatomic,weak) IBOutlet UIImageView *documentImage;
@end
