//
//  Documents.h
//  Doc2Img
//
//  Created by Sean Kovacs on 8/17/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Documents : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * filePath;
@property (nonatomic, retain) NSDate * createdDate;

@end
