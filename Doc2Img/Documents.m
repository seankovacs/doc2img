//
//  Documents.m
//  Doc2Img
//
//  Created by Sean Kovacs on 8/17/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "Documents.h"


@implementation Documents

@dynamic name;
@dynamic filePath;
@dynamic createdDate;

@end
