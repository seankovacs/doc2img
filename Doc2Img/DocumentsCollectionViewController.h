//
//  DocumentsCollectionViewController.h
//  Doc2Img
//
//  Created by Sean Kovacs on 8/17/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentsCollectionViewController : UICollectionViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

- (IBAction)launchCamera:(id)sender;

@end
