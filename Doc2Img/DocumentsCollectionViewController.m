//
//  DocumentsCollectionViewController.m
//  Doc2Img
//
//  Created by Sean Kovacs on 8/17/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "DocumentsCollectionViewController.h"
#import "DocumentCollectionViewCell.h"
#import "ImageEditorViewController.h"
#import "NSObject+PerformBlock.h"
#import "Documents.h"

@interface DocumentsCollectionViewController ()
@property (nonatomic,strong) UILabel *noDocumentsLabel;
@property (nonatomic,strong) NSMutableArray *documents;
@property (nonatomic,strong) UIImagePickerController *cameraController;

- (void)initialSetup;
- (void)refreshCollectionView;
- (void)closeCameraController:(BOOL)animated completion:(void(^)())completion;

@end

@implementation DocumentsCollectionViewController

static NSString * const reuseIdentifier = @"DocumentCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    [self initialSetup];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self refreshCollectionView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Since label is on the nav controller view, we need to hide this once it leaves this controller.
    // Alternative is adding it to the collection view, though there seems to be an autolayout bug with subviews on this
    self.noDocumentsLabel.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Public methods
- (IBAction)launchCamera:(id)sender
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // Show status bar - nicer effect
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    
        // Init the camera can be heavy (take long) - kick it off in the BG so as not to kill the main thread
        [self performBlockOnBGTheadBackgroundPriority:^{
            UIImagePickerController *cameraVC = [[UIImagePickerController alloc] init];
            cameraVC.sourceType = UIImagePickerControllerSourceTypeCamera;
            cameraVC.delegate = self;
    
            self.cameraController = cameraVC;
            // Present camera on main thread
            [self performBlockOnMainThead:^{
                [self presentViewController:self.cameraController animated:YES completion:nil];
            }];

        }];

    }else {
        // No camera detected on this device - show error
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Supported" message:@"This device doesn't appear to have a camera." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark Prviate methods
- (void)initialSetup
{
    // Label to show/hide if no documents are stored/cached
    NSMutableAttributedString *message = [[NSMutableAttributedString alloc] initWithString:@"No Documents\n\nYou can add documents by clicking the camera icon above."];
    NSRange range = NSMakeRange(0, message.string.length);
    [message addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:range];
    [message addAttribute:NSFontAttributeName value:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline] range:range];
    range = [message.string rangeOfString:@"No Documents"];
    [message addAttribute:NSFontAttributeName value:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline] range:range];
    
    self.noDocumentsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width-40.0f, 200.0f)];
    self.noDocumentsLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.noDocumentsLabel.backgroundColor = [UIColor clearColor];
    self.noDocumentsLabel.textAlignment = NSTextAlignmentCenter;
    self.noDocumentsLabel.attributedText = message;
    self.noDocumentsLabel.numberOfLines = 4;
    self.noDocumentsLabel.hidden = YES;

    self.noDocumentsLabel.center = self.navigationController.view.center;
    
    [self.navigationController.view addSubview:self.noDocumentsLabel];
}

- (void)refreshCollectionView
{
    if(!self.documents) self.documents = [NSMutableArray arrayWithCapacity:0];
    
    // Clear the old data (if any)
    [self.documents removeAllObjects];
    
    // Fetch data on BG thread to not hose the UI
    [self performBlockOnBGTheadBackgroundPriority:^{
        NSArray *docs = [Documents MR_findAllSortedBy:@"createdDate" ascending:YES inContext:[NSManagedObjectContext MR_contextForCurrentThread]];
        
        [self performBlockOnMainThead:^{
            [self.documents addObjectsFromArray:docs];
            
            // Display message if no documents exist
            self.noDocumentsLabel.hidden = self.documents.count > 0 ? YES : NO;
            
            [self.collectionView reloadData];
        }];
    }];
}

- (void)closeCameraController:(BOOL)animated completion:(void(^)())completion
{
    if(self.cameraController) {
        [self.cameraController dismissViewControllerAnimated:animated completion:^{
            if(completion) completion();
            
            self.cameraController = nil;
        }];
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.documents.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DocumentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    Documents *document = [self.documents objectAtIndex:indexPath.row];
    
    NSError *error;
    NSData *imageData = [NSData dataWithContentsOfFile:document.filePath options:0 error:&error];
    if(error) NSLog(@"Error: %@",error.description);
    
    UIImage *docImage = [UIImage imageWithData:imageData];
    cell.documentImage.image = docImage;    
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>


#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Mainboard" bundle:[NSBundle mainBundle]];
    ImageEditorViewController *imageVC = [mainSB instantiateViewControllerWithIdentifier:@"ImageEditorViewController"];
    imageVC.sourceImage = originalImage;
    
    // Hide navbar and push image editor to navigation stack
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:imageVC animated:NO];
    
    [self closeCameraController:NO completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self closeCameraController:YES completion:nil];
}

@end
