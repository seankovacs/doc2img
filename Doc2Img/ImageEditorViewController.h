//
//  ImageEditorViewController.h
//  Doc2Img
//
//  Created by Sean Kovacs on 8/17/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CropView.h"
#import <MessageUI/MessageUI.h>

@interface ImageEditorViewController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic,strong) UIImage *sourceImage;
@property (nonatomic,weak) IBOutlet UIImageView *imageView;
@property (nonatomic,weak) IBOutlet CropView *cropView;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic,weak) IBOutlet UIButton *cancelButton;
@property (nonatomic,weak) IBOutlet UIButton *saveButton;

- (IBAction)handlePanning:(UIPanGestureRecognizer *)gesture;
@end
