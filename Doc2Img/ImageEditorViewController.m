//
//  ImageEditorViewController.m
//  Doc2Img
//
//  Created by Sean Kovacs on 8/17/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "ImageEditorViewController.h"
#import "NSObject+PerformBlock.h"
#import "UIImage+Grayscale.h"
#import "Documents.h"

typedef enum {
    CropCornerNone = 0,
    CropCornerTopLeft,
    CropCornerTopRight,
    CropCornerBottomLeft,
    CropCornerBottomRight,
} CropCorner;

@interface ImageEditorViewController ()
@property (nonatomic) CropCorner selectedCropCorner;
@property (nonatomic,strong) UIImage *croppedImage;

- (void)closeEditor;
- (void)cropAndSaveDocument;
- (void)showEmailDocumentUserPrompt;
- (void)emailPicture:(UIImage *)picture;

// Helpers
- (UIImage *)imageByCropping:(UIImage *)image toRect:(CGRect)rect;
@end

@implementation ImageEditorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Create toolbar to cancel/save buttons
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, self.view.bounds.size.height-44.0f, self.view.bounds.size.width, 44.0f)];
    
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(closeEditor)];
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(cropAndSaveDocument)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolBar setItems:@[cancel,flex,save]];
    [self.view addSubview:toolBar];
    
    
    // Start spinner since converting to grayscale takes some millis
    [self.spinner startAnimating];
    self.cropView.hidden = YES;
    
    // Kick off in BG thead to not interrupt UI
    [self performBlockOnBGTheadHighPriority:^{
        // Convert incoming image to grayscale
        UIImage *grayScaleImage = [self.sourceImage convertToGrayscale];
        self.sourceImage = grayScaleImage;
        
        [self performBlockOnMainThead:^{
            [self.spinner stopAnimating];
            self.cropView.hidden = NO;
            
            self.imageView.image = self.sourceImage;
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeEditor
{
    // We are on the navigation stack
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)cropAndSaveDocument
{
    // Start spinner since cropping and writing to disk takes some millis
    [self.spinner startAnimating];
    self.cropView.hidden = YES;
    
    [self performBlockOnBGTheadHighPriority:^{
        // Displayed image is scaled to fit - we need to convert cropped rect of the scaled image to the full size image rect
        
        CGFloat widthScale = self.sourceImage.size.width/self.imageView.frame.size.width;
        CGFloat heightScale = self.sourceImage.size.height/self.imageView.frame.size.height;
        
        CGRect cropRect = CGRectApplyAffineTransform(self.cropView.frame, CGAffineTransformMakeScale(widthScale,heightScale));
        
        // Let UIImage framwork deal with correcting orientation
        self.croppedImage = [self imageByCropping:self.sourceImage toRect:cropRect];
        
        
        // Save image to disk
        NSData *imageData = UIImagePNGRepresentation(self.croppedImage);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-mm-dd hh:MM:ss";
        
        // Doc directory
        NSString *basepath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *filename = [dateFormatter stringFromDate:[NSDate date]];
        NSString *path = [basepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",filename]];
        
        // Write to disk
        [imageData writeToFile:path options:NSDataWritingAtomic error:nil];
        
        [self performBlockOnMainThead:^{
            // Create DB entry
            Documents *doc = [Documents MR_createEntity];
            doc.name = filename;
            doc.createdDate = [NSDate date];
            doc.filePath = path;
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
            
            [self.spinner stopAnimating];
            
            [self showEmailDocumentUserPrompt];
        }];
        
    }];
}

- (void)showEmailDocumentUserPrompt
{
    // Block based wrapper would be ideal :/
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Would you like to email the document?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    alert.delegate = self;
    [alert show];
}

- (void)emailPicture:(UIImage *)picture
{
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailView = [[MFMailComposeViewController alloc] init];
        mailView.mailComposeDelegate = self;
        [mailView.navigationBar setTintColor:UIColorFromRGB(0x82deab)];
        
        [mailView setSubject:@"Document Attached"];
        
        [mailView setMessageBody:@"Document taken with Documents for iOS" isHTML:YES];
        
        NSData *imageData = UIImagePNGRepresentation(self.croppedImage);
        
        [mailView addAttachmentData:imageData mimeType:@"image/png" fileName:@"document.png"];
        
        [self presentViewController:mailView animated:YES completion:nil];
        
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"It doesn't appear you have email setup on this device." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark Helpers

- (UIImage *)imageByCropping:(UIImage *)image toRect:(CGRect)rect
{
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);

    [image drawAtPoint:CGPointMake(-rect.origin.x, -rect.origin.y)];
    
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

#pragma mark AlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self closeEditor];
            break;
        case 1:
            [self emailPicture:self.croppedImage];
            break;
        default:
            break;
    }
}

#pragma mark Mail delegates

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:^{
        [self closeEditor];
    }];
}

#pragma mark Pan Gesture Delegates

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // Determine corner touched
    if([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        self.selectedCropCorner = CropCornerNone;
        
        CGPoint position = [gestureRecognizer locationInView:gestureRecognizer.view.superview];
        CGRect cropRect = gestureRecognizer.view.frame;
        CGFloat radius = 50.0f;
        
        CGFloat distance = powf(position.x - cropRect.origin.x, 2.0f) + powf(position.y - cropRect.origin.y, 2.0f);
        if (distance <= radius * radius){
            self.selectedCropCorner = CropCornerTopLeft;
        }
        
        distance = powf(position.x - (cropRect.origin.x + cropRect.size.width), 2.0f) + powf(position.y - cropRect.origin.y, 2.0f);
        if (distance <= radius * radius){
            self.selectedCropCorner = CropCornerTopRight;
        }
        
        distance = powf(position.x - (cropRect.origin.x + cropRect.size.width), 2.0f) + powf(position.y - (cropRect.origin.y + cropRect.size.height), 2.0f);
        if (distance <= radius * radius){
            self.selectedCropCorner = CropCornerBottomRight;
        }
        
        distance = powf(position.x - cropRect.origin.x, 2.0f) + powf(position.y - (cropRect.origin.y + cropRect.size.height), 2.0f);
        if (distance <= radius * radius){
            self.selectedCropCorner = CropCornerBottomLeft;
        }
    }

    return YES;
}


- (IBAction)handlePanning:(UIPanGestureRecognizer *)gesture
{
    CGRect cropRect = gesture.view.frame;
    
    if(gesture.state == UIGestureRecognizerStateChanged){
        CGPoint dist = [gesture translationInView:gesture.view.superview];
        
        switch (self.selectedCropCorner) {
            case CropCornerTopLeft:
                cropRect.origin.x += dist.x;
                cropRect.origin.y += dist.y;
                cropRect.size.width -= dist.x;
                cropRect.size.height -= dist.y;
                
                gesture.view.frame = cropRect;
                break;
            case CropCornerTopRight:
                cropRect.origin.y += dist.y;
                cropRect.size.width += dist.x;
                cropRect.size.height -= dist.y;
                
                gesture.view.frame = cropRect;
                break;
            case CropCornerBottomLeft:
                cropRect.origin.x += dist.x;
                cropRect.size.width -= dist.x;
                cropRect.size.height += dist.y;
                
                gesture.view.frame = cropRect;
                break;
            case CropCornerBottomRight:
                cropRect.size.width += dist.x;
                cropRect.size.height += dist.y;
                
                gesture.view.frame = cropRect;
                break;
            default:
                break;
        }
        
        [gesture setTranslation:CGPointZero inView:gesture.view];
    }
}
@end
