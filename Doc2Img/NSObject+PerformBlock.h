//
//  NSObject+PerformBlock.h
//  gvmobile
//
//  Created by Sean Kovacs on 12/18/12.
//  Copyright (c) 2012 SK DEV Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (PerformBlock)

- (void)performBlock:(void (^)())block afterDelay:(NSTimeInterval)delay;
- (void)performBlockOnMainThead:(void (^)())block;
- (void)performBlockOnBGThead:(void (^)())block afterDelay:(NSTimeInterval)delay;
- (void)performBlockOnBGTheadBackgroundPriority:(void (^)())block;
- (void)performBlockOnBGTheadHighPriority:(void (^)())block;
@end
