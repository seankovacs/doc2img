//
//  NSObject+PerformBlock.m
//  gvmobile
//
//  Created by Sean Kovacs on 12/18/12.
//  Copyright (c) 2012 SK DEV Solutions, LLC. All rights reserved.
//

#import "NSObject+PerformBlock.h"

@implementation NSObject (PerformBlock)

- (void)performBlock:(void (^)())block afterDelay:(NSTimeInterval)delay
{
    double delayInSeconds = delay;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (block) {
            block();
        }
    });
}

- (void)performBlockOnMainThead:(void (^)())block
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (block) {
            block();
        }
    });
}

- (void)performBlockOnBGThead:(void (^)())block afterDelay:(NSTimeInterval)delay
{
    double delayInSeconds = delay;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void){
        if (block) {
            block();
        }
    });
}

- (void)performBlockOnBGTheadBackgroundPriority:(void (^)())block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if (block) {
            block();
        }
    });
}

- (void)performBlockOnBGTheadHighPriority:(void (^)())block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        if (block) {
            block();
        }
    });
}

@end
