//
//  UIImage+Grayscale.h
//  Doc2Img
//
//  Created by Sean Kovacs on 8/19/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Grayscale)

- (UIImage *)convertToGrayscale;

@end
