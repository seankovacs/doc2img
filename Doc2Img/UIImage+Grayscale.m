//
//  UIImage+Grayscale.m
//  Doc2Img
//
//  Created by Sean Kovacs on 8/19/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "UIImage+Grayscale.h"

@implementation UIImage (Grayscale)

- (UIImage *)convertToGrayscale
{
    CIImage *beginImage = [CIImage imageWithCGImage:self.CGImage];
    
    CIImage *output = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, beginImage, @"inputIntensity", [NSNumber numberWithFloat:1.0], @"inputColor", [[CIColor alloc] initWithColor:[UIColor whiteColor]], nil].outputImage;
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgiimage = [context createCGImage:output fromRect:output.extent];
    UIImage *newImage = [UIImage imageWithCGImage:cgiimage scale:self.scale orientation:self.imageOrientation];
    
    CGImageRelease(cgiimage);
    
    return newImage;
}

@end
