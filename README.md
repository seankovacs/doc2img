# README #

Doc2Img (aka Documents) is a simple camera app that allows for cropping and emailing of documents.

Each document is stored and displayed in a beautiful grid view (UICollectionView).